<?php

/**
 * Description of AnswerInterface
 *
 * @author koodikoulu
 */

namespace Survey\SurveyPage\Api\Data;

interface AnswerInterface {

    const ID = 'answer_id';
    const NAME = 'answer_name';
    const EMAIL = 'answer_email';
    const TEXT = 'answer_text';
    const CREATED_AT = 'answer_created_at';
    
    public function getId();    
    public function setId($id);

    public function getName();
    public function setName($name);
    
    public function getEmail();
    public function setEmail($email);
    
    public function getText();
    public function setText($text);
    
    public function getCreatedAt();
    public function setCreatedAt($createdAt);
}
