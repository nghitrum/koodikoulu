<?php

namespace Survey\SurveyPage\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Widget\Block\BlockInterface;

class AnswerWidget extends Template implements BlockInterface {

    protected $_template = "answerwidget.phtml";

    public function __construct(
            Context $context,
            \Survey\SurveyPage\Model\ResourceModel\Answer\CollectionFactory $model
            ) {
        $this->model = $model;
        parent::__construct($context);
    }

    public function GetResults() {
        $answerCollection = $this->model->create();
        return $answerCollection;
    }

}
