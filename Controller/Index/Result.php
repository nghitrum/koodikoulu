<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Survey\SurveyPage\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Element\Messages;
use Magento\Framework\View\Result\PageFactory;

class Result extends Action {

    /** @var PageFactory $resultPageFactory */
    protected $resultPageFactory;

    private $answerFactory;
    
    /**
     * Result constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
            Context $context,
            PageFactory $pageFactory,
            \Survey\SurveyPage\Model\AnswerFactory $answerFactory
            ) {
        $this->resultPageFactory = $pageFactory;
        $this->answerFactory = $answerFactory;
        parent::__construct($context);
    }

    /**
     * The controller action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute() {
        $name = $this->getRequest()->getParam('name');
        $email = $this->getRequest()->getParam('email');
        $answer = $this->getRequest()->getParam('answer');
        
        $errorCount = 0;

        $resultPage = $this->resultPageFactory->create();

        /** @var Messages $messageBlock */
        $messageBlock = $resultPage->getLayout()->createBlock(
                'Magento\Framework\View\Element\Messages', 'answer'
        );

        //  validate inputs
        //  name
        if (strlen($name) > 0) {
            $messageBlock->addSuccess('Your name is <strong>' . $name);
        } else {
            $messageBlock->addError('Name is required.');
            $errorCount ++;
        }

        //  email
        if (strlen($email) > 0) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $messageBlock->addError("Invalid email format");
                $errorCount ++;
            } else {
                $messageBlock->addSuccess('Your email is <strong>' . $email . '</strong>');
            }
        } else {
            $messageBlock->addError('Email is required.');
            $errorCount ++;
        }

        //  answer
        if (strlen($answer) > 0) {
            $messageBlock->addSuccess('Your answer is <strong>' . $answer . '</strong>');
        } else {
            $messageBlock->addError('Answer is required.');
            $errorCount ++;
        }

        $resultPage->getLayout()->setChild(
                'content', $messageBlock->getNameInLayout(), 'answer_alias'
        );
        
        if ($errorCount === 0) {
            $model = $this->answerFactory->create();
            $model->setName($name);
            $model->setEmail($email);
            $model->setText($answer);
            $model->setCreatedAt(date('Y-m-d H:i:s'));
            
            $model->save();
            $messageBlock->addSuccess('<strong>Your data has been inserted to database.</strong>');
        }

        return $resultPage;
    }

}
