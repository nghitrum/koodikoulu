<?php

namespace Survey\SurveyPage\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class Recurring implements InstallSchemaInterface
{
    const ANSWER_TABLE = 'survey_answer';
    const ANSWER_ID = 'answer_id';
    const ANSWER_NAME = 'answer_name';
    const ANSWER_EMAIL = 'answer_email';
    const ANSWER_TEXT = 'answer_text';
    const ANSWER_CREATED_AT = 'answer_created_at';

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $coreResource;

    /**
     * @param \Magento\Framework\App\ResourceConnection $coreResource
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $coreResource
    ) {
        $this->coreResource = $coreResource;
    }

    /**
     * Stub for recurring setup script.
     *
     * For quick development and prototyping purposes we re-create our tables during every call to setup:upgrade,
     * regardless of version upgrades, which you normally would not do in production.
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $setup
     * @throws
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        return;
        $setup->startSetup();

        $connection = $this->coreResource->getConnection();

        // Drop existing tables for quick prototyping. Comment out if you don't want this!
        if ($connection->isTableExists(self::ANSWER_TABLE)) {
            $connection->dropTable(self::ANSWER_TABLE);
        }

        $surveyTable = $connection->newTable(
            self::ANSWER_TABLE
        )->addColumn(
            self::ANSWER_ID,
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Answer ID'

            // build up your DB schema here
        )->addColumn(
        self::ANSWER_NAME,
            Table::TYPE_TEXT,
            null,
            ['nullable' => false], 
            'Answer Name'
        )->addColumn(
        self::ANSWER_EMAIL,
            Table::TYPE_TEXT,
            null,
            ['nullable' => false], 
            'Answer Email'
        )->addColumn(
        self::ANSWER_TEXT,
            Table::TYPE_TEXT,
            null,
            ['nullable' => false], 
            'Answer Text'
        )->addColumn(
        self::ANSWER_CREATED_AT,
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
            'Created At'
        );

        $connection->createTable($surveyTable);

        $setup->endSetup();
    }
}