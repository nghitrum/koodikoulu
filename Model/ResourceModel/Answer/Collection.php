<?php

/**
 * Description of Collection
 *
 * @author koodikoulu
 */

namespace Survey\SurveyPage\Model\ResourceModel\Answer;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {
    
    /**
     * Remittance File Collection Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Survey\SurveyPage\Model\Answer::class, \Survey\SurveyPage\Model\ResourceModel\Answer::class);
    }
}
