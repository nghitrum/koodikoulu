<?php

/**
 * Description of Answer
 *
 * @author koodikoulu
 */
namespace Survey\SurveyPage\Model;

use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;
use Survey\SurveyPage\Api\Data\AnswerInterface;

class Answer extends AbstractModel implements AnswerInterface, IdentityInterface {
     /**
     * Post Abstract Resource Constructor
     * @return void
     */
    protected function _construct() {
        $this->_init(\Survey\SurveyPage\Model\ResourceModel\Answer::class);
    }
    
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getId() {
        return $this->getData(self::ID);
    }

    public function setId($id) {
        return $this->setData(self::ID, $id);
    }

    public function getName() {
        return $this->getData(self::NAME);
    }

    public function setName($name) {
        return $this->setData(self::NAME, $name);
    }

    public function getEmail() {
        return $this->getData(self::EMAIL);
    }

    public function setEmail($email) {
        return $this->setData(self::EMAIL, $email);
    }

    public function getText() {
        return $this->getData(self::TEXT);
    }

    public function setText($text) {
        return $this->setData(self::TEXT, $text);
    }

    public function getCreatedAt() {
        return $this->getData(self::CREATED_AT);
    }

    public function setCreatedAt($createdAt) {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
